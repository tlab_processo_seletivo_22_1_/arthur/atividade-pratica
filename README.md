# [TLAB] Processo Seletivo 22.1

## Arthur Mayan
<br>
<h3>Questão 3</h3>
<p>
	Esse código realiza a simulação de uma calculadora, realizando a soma de dois valores disponibilizados pelo usuario através do terminal.
	<br>Para compilar o codigo em JavaScript:
	<br>node < nomeDoPrograma >  < entrada1 >  < entrada2 > 
	<br>node calculadora.js 2 2
	<br>Resultado: 4
</p>

<br>
<br>

<h3>Questão 4</h3>
<p>
	Para subir um código para o repositorio sem usar "git add ." é utilizado o comando "git add < nomeDoArquivo >", dessa forma é enviada apenas o arquivo desejado, e não todas as alterações.
	<br>Sendo assim, é realizados os seguintes comandos:
	<br>git add calculadora.js
	<br>git add README.md
</p>

<br>
<br>

<h3>Questão 6</h3>
<p>
	Contém dois erros nesse codigo.
	<br>1 - Na linha 21, está escrito arg[0], faltando o " S ". Corrigindo ficaria: args[0];
	<br>2 - Os Parametros passados deveriam ser args[1] e args[2], ao invés de  args[0] e args[1], pois no args[0] está sendo armazenado a opção para o switch, sendo definida qual operação a calculadora deveria realizar.
</p>


